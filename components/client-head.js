import Head from 'next/head'

const ClientHead = ({
  portalMeta,
  dogImageUrl,
}) => {
  return <Head>
    <title>Portal show</title>
    <link rel="icon" href="/favicon.ico" />
    {portalMeta && portalMeta.description &&
      <meta property="description" content={portalMeta.description} key="metadescription"/>
    }
    {dogImageUrl &&
      <meta property="description" content="Stateful runtime description" key="metadescription"/>
    }
    {portalMeta && portalMeta.ogsite_name &&
      <meta property="og:site_name" content={portalMeta.ogsite_name} key="metaogsite_name"/>
    }
    {portalMeta && portalMeta.ogurl &&
      <meta property="og:url" content={portalMeta.ogurl} key="metaogurl"/>
    }
    {portalMeta && portalMeta.ogdescription &&
      <meta property="og:description" content={portalMeta.ogdescription} key="metaogdescription"/>
    }
    {portalMeta && portalMeta.ogimage &&
      <meta property="og:image" content={portalMeta.ogimage} key="metaogimage"/>
    }
    {portalMeta && portalMeta.twitterurl &&
      <meta property="twitter:url" content={portalMeta.twitterurl} key="metatwitterurl"/>
    }
    {portalMeta && portalMeta.twittercard &&
      <meta property="twitter:card" content={portalMeta.twittercard} key="metatwittercard"/>
    }
    {portalMeta && portalMeta.twitterimagealt &&
      <meta property="twitter:image:alt" content={portalMeta.twitterimagealt} key="metatwitterimagealt"/>
    }
    {portalMeta && portalMeta.fbapp_id &&
      <meta property="fb:app_id" content={portalMeta.fbapp_id} key="metafbapp_id"/>
    }
  </Head>
}

export default ClientHead
