import { useEffect } from 'react'
import { useRouter } from 'next/router'
import Head from 'next/head'
// Helpers
import { getAllOrganizationPaths } from '../../../lib/static-paths'

const OrganizationShow = ({
  organizationSlug
}) => {
  const router = useRouter()

  useEffect(() => {
    let mounted = true
    if (mounted) {
      console.log ('OrganizationShow is firing', router)
    }
    return () => mounted = false
  }, [router.query])

  return <>
    <Head>
      <title>Organization Show</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>
    <div className='page'>
      <h1>OrganizationShow</h1>
      <h3>organization_slug: {organizationSlug}</h3>
    </div>
  </>
}

export default OrganizationShow

export async function getStaticProps(context) {
  console.log ('OrganizationSlug getStaticProps')
  const {organization_slug} = context.params
  console.log ('organization_slug', organization_slug)
  // TODO: Get all of an organization's portals
  // Then check for an "index portal" (not yet implemented)
  // If one exists, this page is effectively the /[portal_slug] for that portal
  return {
    props: {
      organizationSlug: organization_slug,
    }
  }
}
export async function getStaticPaths() {
  const paths = getAllOrganizationPaths()
  return { paths, fallback: true }
}
