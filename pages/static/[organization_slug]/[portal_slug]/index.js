import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
// Components
import ClientHead from '../../../../components/client-head'
// Helpers
import { getAllPortalPaths } from '../../../../lib/static-paths'

const PortalShow = ({
  portalMeta,
}) => {
  const [dogImageUrl, setDogImageUrl] = useState(null)
  const [loading, setLoading] = useState(true)
  const router = useRouter()

  useEffect(() => {
    let mounted = true
    if (mounted) {
      console.log ('PortalShow is firing', router)
      console.log ('portalMeta', portalMeta)
      setLoading(true)
      const getDogImage = async () => {
        try {
          const fetchUrl = 'https://random.dog/woof.json'
          const dogImageResponse = await fetch(fetchUrl)
          if (!dogImageResponse.ok)
            throw new Error(`${dogImageResponse.status} -- Unknown API issue`)

          const dogImage = await dogImageResponse.json()
          return dogImage
        } catch (error) {
          // console.error ('error', error)
          throw new Error(`${error} -- Couldn't reach API`)
        }
      }
      getDogImage()
      .then(dogImage => {
        setDogImageUrl(dogImage && dogImage.url)
      })
      .finally(() => setLoading(false))
    }
    return () => mounted = false
  }, [router.query])

  return <>
    <ClientHead
      portalMeta={portalMeta}
      dogImageUrl={dogImageUrl}
    />
    {loading
      ? <div>Loading...</div>
      : <>
        <div className='page'>
          <h1>PortalShow</h1>
          {dogImageUrl &&
            <img
              src={dogImageUrl}
              alt='a beautiful dog laying down'
              style={{
                height: '200px',
              }}
            />
          }
        </div>
      </>
    }
  </>
}

export default PortalShow

export async function getStaticProps(context) {
  console.log ('PortalShow getStaticProps', context)
  const {organization_slug, portal_slug} = context.params
  console.log ('organization_slug', organization_slug)
  console.log ('portal_slug', portal_slug)
  const prefix = organization_slug === 'sandiegoreader' ? 'sd -' : 'sctk -'
  const ogimage = (
    organization_slug === 'sandiegoreader'
    ? 'https://ucarecdn.com/402068c7-8969-4246-b221-1480c4fe9733/'
    : 'https://ucarecdn.com/dae86e3c-c4d6-49ac-8bab-a2beefe6ab66/'
  )
  const metaurl = (
    organization_slug === 'sandiegoreader'
  ? 'https://www.sandiegobestof.com'
  : 'https://www.scenethink.com'
  )
  const appId = (
    organization_slug === 'sandiegoreader'
    ? '966242223397117'
    : '613576348806572'
  )
  const portalMeta = {
    description: `${prefix} Description`,
    ogimage: ogimage,
    ogtitle: `${prefix} Title`,
    ogdescription: `${prefix} Description`,
    ogurl: metaurl,
    ogsite_name: `${prefix} Site name`,
    twitterurl: metaurl,
    twittercard: 'photo',
    twitterimagealt: `${prefix} image alt`,
    fbapp_id: appId,
  }
  // TODO: Get all of an organization's portals
  // Then check for an "index portal" (not yet implemented)
  // If one exists, this page is effectively the /[portal_slug] for that portal
  return {
    props: {
      portalMeta,
    }
  }
}
export async function getStaticPaths() {
  const paths = getAllPortalPaths()
  return { paths, fallback: true }
}
