// next.config.js
module.exports = {
  async rewrites() {
    // When the user visits:        vercel.app/sandiegoreader/best-of-sd-2020
    // The user will get served:    vercel.app/static/sandiegoreader/best-of-sd-2020
    return [
      {
        source: "/:slugs*",
        destination: "/static/:slugs*"
      },
    ]
    // We still need to do this:
    // When the user visits:        sandiegobestof.com/best-of-sd-2020
    // The user will get served:    vercel.app/sandiegoreader/best-of-sd-2020
    // return [
    //   {
    //     source: "/:slugs*",
    //     destination: "/api/switchboard/:slugs*"
    //   },
    // ]
  },
}
