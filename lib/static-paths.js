// export async function getAllOrganizationPaths() {} OR
// export const getAllOrganizationPaths = async () => {}
export const getAllOrganizationPaths = () => {
  return [
    {
      params: {
        organization_slug: 'sandiegoreader',
      },
    },
    {
      params: {
        organization_slug: 'scenethink',
      }
    }
  ]
}
// export async function getAllPortalPaths() {} OR
// export const getAllPortalPaths = async () => {}
export const getAllPortalPaths = () => {
  return [
    {
      params: {
        organization_slug: 'sandiegoreader',
        portal_slug: 'san-diego-best-of',
      },
    },
    {
      params: {
        organization_slug: 'scenethink',
        portal_slug: 'best-of',
      }
    }
  ]
}
